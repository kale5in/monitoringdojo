import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { Sentry } from './page/sentry/sentry'
import { RollBar } from './page/rollbar/rollbar';
import { TrackJs } from './page/trackjs/trackjs';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit and save to reload.
        </p>
        <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/sentry">Sentry</Link>
            </li>
            <li>
              <Link to="/rollbar">RollBar</Link>
            </li>
            <li>
              <Link to="/trackjs">TrackJs</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/sentry">
            <Sentry />
          </Route>
          <Route path="/rollbar">
            <RollBar />
          </Route>
          <Route path="/trackjs">
            <TrackJs />
          </Route>
        </Switch>
      </div>
    </Router>
      </header>
    </div>
  );
}

export default App;
