import React, { Component } from "react";
import { TrackJS } from "trackjs";

export class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = { error: null };
    }

    componentDidCatch(error, errorInfo) {
        if (errorInfo && errorInfo.componentStack) {
            console.log(errorInfo.componentStack);
        }

        TrackJS.track(error);
        this.setState({ error });
    }

    render() {
        if (this.state.error) {
            return <h1>Something went wrong.</h1>;
          }
      
          return this.props.children;
    }
}