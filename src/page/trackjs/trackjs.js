import React from 'react';
import { ErrorBoundary } from './errorBoundary';

const TrackjsPage = () => {
    const handleMakeError = () => {
        throw  new Error("Mother fucker")
    }
    return <div>
        <button onClick={handleMakeError}>
            I'm error trackjs
        </button>
    </div>
}


export const TrackJs = () => {
    return (
        <ErrorBoundary>
            <TrackjsPage/>
        </ErrorBoundary>
    )
}