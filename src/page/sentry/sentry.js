import React from 'react';
import { ErrorBoundary } from './errorBoundary';

const SentryPage = () => {
    const handleMakeError = () => {
        throw  new Error("Mother fucker")
    }
    return <div>
        <button onClick={handleMakeError}>
            I'm error sentry
        </button>
    </div>
}


export const Sentry = () => {
    return (
        <ErrorBoundary>
            <SentryPage/>
        </ErrorBoundary>
    )
}