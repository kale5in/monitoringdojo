import React from 'react';
import { ErrorBoundary } from './errorBoundary';

const RollBarPage = () => {
    const handleMakeError = () => {
        throw  new Error("Mother fucker")
    }
    return <div>
        <button onClick={handleMakeError}>
            I'm error rollbar
        </button>
    </div>
}


export const RollBar = () => {
    return (
        <ErrorBoundary>
            <RollBarPage/>
        </ErrorBoundary>
    )
}