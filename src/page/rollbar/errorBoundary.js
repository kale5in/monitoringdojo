import React from 'react';
import Rollbar from "rollbar";

export class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        rollbar: new Rollbar({
          accessToken: 'b646ab798c0941d6bee3f9952c443253',
          captureUncaught: true,
          captureUnhandledRejections: true,
        }),
        hasError: false
      };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    this.state.rollbar.info(error.message);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}

