import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as Sentry from '@sentry/browser';
import { TrackJS } from "trackjs";

Sentry.init({dsn: "https://10424b6197a9421283c3c284018dbd48@sentry.io/1829323"});

TrackJS.install({
  token: "ae334953cf8646b291d26d5df9180c48"
});


ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
